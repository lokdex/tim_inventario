from ..models import db, DetalleStock, MaestroProducto
from sqlalchemy.exc import DBAPIError
from decimal import Decimal
from sqlalchemy import and_


class CDetalleStock:
    error = None

    def valParams(self, params={}):
        if (not params["ccodsuc"] or
            not params["ccodpro"] or
                not params["nstock"]):
            self.error = "NO SE INGRESARON TODOS LOS DATOS"
            return False
        if (len(params["ccodsuc"]) != 3 or
            len(params["ccodpro"]) != 8 or
            float(params["nstock"]) < 0 or
                float(params["nstock"]) > 9999.99):
            self.error = "FORMATO INCORRECTO DE LOS DATOS"
            return False
        return True

    def search(self, params={}):
        try:
            productos = db.session.query(
                DetalleStock.nstock.label("nstock"), 
                MaestroProducto.cdescri.label("cdescri")).filter(
                DetalleStock.ccodpro == MaestroProducto.ccodpro,
                DetalleStock.ccodsuc == params["ccodsuc"],
                MaestroProducto.cdescri.like("%{}%".format(params["cdescri"]))).all()
            productos = [{"cdescri":i.cdescri, "nstock": float(i.nstock)} for i in productos]
        except:
            productos = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return productos if productos else []

    def ingreso(self, params={}):
        try:
            detalle_stock = self.find(params)
            if not detalle_stock:
                self.error = "NO EXISTE EL PRODUCTO"
                return False
            if detalle_stock.nstock - Decimal(params["ncantid"]) > 9999.99:
                self.error = "EXCEDE LA CANTIDAD LIMITE"
                return False
            detalle_stock.nstock += Decimal(params["ncantid"])
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def salida(self, params={}):
        try:
            detalle_stock = self.find(params)
            if not detalle_stock:
                self.error = "NO EXISTE EL PRODUCTO"
                return False
            if detalle_stock.nstock - Decimal(params["ncantid"]) < 0:
                self.error = "NO HAY LA CANTIDAD SUFICIENTE"
                return False
            detalle_stock.nstock -= Decimal(params["ncantid"])
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def find(self, params={}):
        try:
            detalle_stock = db.session.query(DetalleStock).filter(
                DetalleStock.ccodsuc == (params["ccodsuc"]),
                DetalleStock.ccodpro == (params["ccodpro"])).first()
        except:
            detalle_stock = {}
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return detalle_stock if detalle_stock else {}

    def findSuc(self, params={}):
        try:
            detalle_stock = db.session.query(
                DetalleStock.nstock.label("nstock"), 
                MaestroProducto.cdescri.label("cdescri")).filter(
                DetalleStock.ccodpro == MaestroProducto.ccodpro).filter(
                DetalleStock.ccodsuc == params["ccodsuc"]).all()
            detalle_stock = [{"cdescri":i.cdescri, "nstock": float(i.nstock)} for i in detalle_stock]
        except:
            detalle_stock = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return detalle_stock if detalle_stock else []

    def insert(self, params={}):
        if not self.valParams(params):
            return
        try:
            new_detalle_stock = DetalleStock(
                ccodsuc=params["ccodsuc"],
                ccodpro=params["ccodpro"],
                nstock=params["nstock"])
            db.session.add(new_detalle_stock)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def update(self, params={}):
        if not self.valParams(params):
            return
        try:
            detalle_stock = DetalleStock.query.get(params["nserial"])
            detalle_stock.cidmovi = params["ccodsuc"]
            detalle_stock.ccodpro = params["ccodpro"]
            detalle_stock.ncantid = params["nstock"]
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def delete(self, params={}):
        try:
            detalle_stock = DetalleStock.query.get(params["nserial"])
            db.session.delete(detalle_stock)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
