from ..models import db, MaestroInventario

class CMaestroInventario:
    error = None

    def valParams(self, params={}):
        if (not params["ccodsuc"] or
            not params["dfecha"]  or
            not params["cdescri"] or
            not params["ctipmov"]):
            self.error = "NO SE INGRESARON TODOS LOS DATOS"
            return False
        if (len(params["ccodsuc"]) != 3 or
            len(params["cdescri"]) < 5 or
            len(params["cdescri"]) > 200 or
            len(params["ctipmov"]) != 1 or
            params["ctipmov"] not in ("I","S")):
            self.error = "FORMATO INCORRECTO DE LOS DATOS"
            return False
        return True

    def select(self):
        try:
            movimientos = MaestroInventario.query.all()
        except:
            movimientos = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return movimientos
    
    def find(self):
        try:
            movimiento = db.session.query(MaestroInventario).order_by(
                MaestroInventario.cidmovi.desc()).first()
        except:
            movimiento = {}
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return movimiento.cidmovi if movimiento else {}

    def search(self, params={}):
        try:
            movimiento = MaestroInventario.query.get(params["cidmovi"])
        except:
            movimiento = {}
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return movimiento if movimiento else {}
    
    def insert(self, params={}):
        if not self.valParams(params):
            return
        try:
            new_movimiento = MaestroInventario(
                ccodsuc = params["ccodsuc"],
                dfecha  = params["dfecha"],
                cdescri = params["cdescri"],
                ctipmov = params["ctipmov"])
            db.session.add(new_movimiento)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def update(self, params={}):
        if not self.valParams(params):
            return
        try:
            movimiento = MaestroInventario.query.get(params["cidmovi"])
            movimiento.ccodsuc = params["ccodsuc"]
            movimiento.dfecha  = params["dfecha"]
            movimiento.cdescri = params["cdescri"]
            movimiento.ctipmov = params["ctipmov"]
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return
    
    def delete(self, params={}):
        try:
            movimiento = MaestroInventario.query.get(params["cidmovi"])
            db.session.delete(movimiento)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
