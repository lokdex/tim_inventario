
from ..models import db, func, MaestroProducto


class CMaestroProducto:
    error = None

    def valParams(self, params={}):
        if (not params["ccodpro"] or
            not params["cdescri"] or
            not params["cunidad"]):
            self.error = "NO SE INGRESARON TODOS LOS DATOS"
            return False
        if (len(params["ccodpro"]) != 8 or
            len(params["cdescri"]) < 5 or
            len(params["cdescri"]) > 200 or
            len(params["cunidad"]) < 1 or
            len(params["cunidad"]) > 3):
            self.error = "FORMATO INCORRECTO DE LOS DATOS"
            return False
        return True

    def select(self):
        try:
            productos = MaestroProducto.query.all()

            if not productos:
                self.error = "NO HAY PRODUCTOS DISPONIBLES"
        except:
            productos = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return productos if productos else []
    
    def selectActive(self):
        try:
            productos = MaestroProducto.query.filter(MaestroProducto.cestado == "A").all()

            if not productos:
                self.error = "NO HAY PRODUCTOS DISPONIBLES"
        except:
            productos = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return productos if productos else []
    
    def insert(self, params={}):
        if not self.valParams(params):
            return
        try:
            new_producto = MaestroProducto(
                ccodpro = params["ccodpro"],
                cdescri = params["cdescri"],
                cunidad = params["cunidad"],
                cestado = "A")
            db.session.add(new_producto)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def update(self, params={}):
        if not self.valParams(params):
            return
        try:
            producto = MaestroProducto.query.get(params["ccodpro"])

            if not producto:
                self.error = "NO EXISTE PRODUCTO"
                return
            
            producto.cdescri = params["cdescri"]
            producto.cunidad = params["cunidad"]
            producto.tmodifi = func.now()
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def changeState(self, params={}):
        try:
            producto = MaestroProducto.query.get(params["ccodpro"])

            if not producto:
                self.error = "NO EXISTE PRODUCTO"
                return
            
            if producto.cestado == "A":
                producto.cestado = "I"

            elif producto.cestado == "I":
                producto.cestado = "A"

            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
