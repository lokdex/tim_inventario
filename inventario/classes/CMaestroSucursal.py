from ..models import db, MaestroSucursal

class CMaestroSucursal:
    error = None

    def valParams(self, params={}):
        if (not params["ccodsuc"] or
            not params["cdescri"]):
            self.error = "NO SE INGRESARON TODOS LOS DATOS"
            return False
        if (len(params["ccodsuc"]) != 3 or
            len(params["cdescri"]) < 5 or
            len(params["cdescri"]) > 200):
            self.error = "FORMATO INCORRECTO DE LOS DATOS"
            return False
        return True
    
    def select(self):
        try:
            sucursales = MaestroSucursal.query.all()
            if not sucursales:
                self.error = "NO HAY SUCURSALES DISPONIBLES"
        except:
            sucursales = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return sucursales if sucursales else []

    def selectActive(self):
        try:
            sucursales = MaestroSucursal.query.filter(MaestroSucursal.cestado == "A").all()
            if not sucursales:
                self.error = "NO HAY SUCURSALES DISPONIBLES"
        except:
            sucursales = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return sucursales if sucursales else []
    
    def insert(self, params={}):
        if not self.valParams(params):
            return
        new_sucursal = MaestroSucursal(
            ccodsuc = params["ccodsuc"],
            cdescri = params["cdescri"],
            cestado = "A")
        try:
            db.session.add(new_sucursal)
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"

    def update(self, params={}):
        if not self.valParams(params):
            return
        try:
            sucursal = MaestroSucursal.query.get(params["ccodsuc"])

            if not sucursal:
                self.error = "NO EXISTE SUCURSAL"
                return
            
            sucursal.cdescri = params["cdescri"]
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
    
    def changeState(self, params={}):
        try:
            sucursal = MaestroSucursal.query.get(params["ccodsuc"])

            if not sucursal:
                self.error = "NO EXISTE SUCURSAL"
                return

            if sucursal.cestado == "A":
                sucursal.cestado = "I"

            elif sucursal.cestado == "I":
                sucursal.cestado = "A"
            
            db.session.commit()
        except:
            db.session.rollback()
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"