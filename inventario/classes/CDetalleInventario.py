from ..models import db, DetalleInventario, MaestroProducto

class CDetalleInventario:
    error = None

    def valParams(self, params={}):
        if (not params["cidmovi"] or
            not params["ccodpro"] or
            not params["ncantid"]):
            self.error = "NO SE INGRESARON TODOS LOS DATOS"
            return False
        if (len(params["ccodpro"]) != 8 or
            float(params["ncantid"]) < 0 or
            float(params["ncantid"]) > 9999.99):
            self.error = "FORMATO INCORRECTO DE LOS DATOS"
            return False
        return True

    def select(self, params={}):
        try:
            detalles_movimiento = db.session.query(DetalleInventario).filter(
                DetalleInventario.cidmovi == params["cidmovi"]).order_by(DetalleInventario.nserial.desc()).all()
        except:
            detalles_movimiento = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return detalles_movimiento if detalles_movimiento else []
    
    def find(self, params={}):
        try:
            detalles_movimiento = db.session.query(
                MaestroProducto.cdescri.label("cdescri"),
                DetalleInventario.ncantid.label("ncantid")).filter(
                DetalleInventario.ccodpro == MaestroProducto.ccodpro,
                DetalleInventario.cidmovi == params["cidmovi"]).all()
            detalles_movimiento = [{"cdescri":i.cdescri, "ncantid": float(i.ncantid)} for i in detalles_movimiento]
        except:
            detalles_movimiento = []
            self.error = "ERROR AL REALIZAR LA OPERACIÓN"
        return detalles_movimiento if detalles_movimiento else []

    def insert(self, params={}):
        if not self.valParams(params):
            return
        new_detalle_movimiento = DetalleInventario(
            cidmovi = params["cidmovi"], 
            ccodpro = params["ccodpro"], 
            ncantid = params["ncantid"])
        db.session.add(new_detalle_movimiento)
        db.session.commit()
        return

    def update(self, params={}):
        if not self.valParams(params):
            return
        detalle_movimiento = DetalleInventario.query.get(params["nserial"])
        detalle_movimiento.cidmovi = params["cidmovi"]
        detalle_movimiento.ccodpro = params["ccodpro"] 
        detalle_movimiento.ncantid = params["ncantid"]
        db.session.commit()
        return
    
    def delete(self, params={}):
        detalle_movimiento = DetalleInventario.query.get(params["nserial"])
        db.session.delete(detalle_movimiento)
        db.session.commit()
        return