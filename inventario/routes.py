"""Route declaration."""
from flask import current_app as app
import json
from flask import render_template, request, redirect, url_for
from .encoder import AlchemyEncoder

from .classes.CDetalleStock import CDetalleStock
from .classes.CMaestroSucursal import CMaestroSucursal
from .classes.CMaestroProducto import CMaestroProducto
from .classes.CMaestroInventario import CMaestroInventario
from .classes.CDetalleInventario import CDetalleInventario

################## Views ###################
# the selects work with some dark fuckery


@app.route('/')
def home():
    """Landing page."""
    return render_template("home.html")

##########################  DB RELATED ####################################

# ------------------------CRUD MAESTRO SUCURSAL-------------------------------


@app.route('/mantenimiento-sucursales', methods=['GET'])
def mantenimientoSucursales():
    oSql = CMaestroSucursal()
    sucursales = oSql.select()
    error = oSql.error
    if error:
        return render_template("mantenimientoSucursales.html",
                            sucursales=sucursales, error=error)
    return render_template("mantenimientoSucursales.html",
                            sucursales=sucursales)


@app.route('/mantenimiento-sucursales/select', methods=['POST'])
def selectSucursales():
    oSql = CMaestroSucursal()
    sucursales = oSql.select()
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    return json.dumps(sucursales, cls=AlchemyEncoder)

@app.route('/mantenimiento-sucursales/select-active', methods=['POST'])
def selectActiveSucursales():
    oSql = CMaestroSucursal()
    sucursales = oSql.selectActive()
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    return json.dumps(sucursales, cls=AlchemyEncoder)

@app.route('/mantenimiento-sucursales/insert', methods=['POST'])
def insertSucursal():
    params = {
        "ccodsuc": request.form.get("ccodsuc"),
        "cdescri": request.form.get("cdescri")
    }
    oSql = CMaestroSucursal()
    oSql.insert(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/mantenimiento-sucursales/update', methods=['POST'])
def updateSucursal():
    params = {
        "ccodsuc": request.form.get("ccodsuc"),
        "cdescri": request.form.get("cdescri")
    }
    oSql = CMaestroSucursal()
    oSql.update(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/mantenimiento-sucursales/change-state', methods=['POST'])
def changeStateSucursal():
    params = {
        "ccodsuc": request.form.get("ccodsuc")
    }
    oSql = CMaestroSucursal()
    oSql.changeState(params)
    error = oSql.error
    return json.dumps({"error": error})

# ------------------------CRUD MAESTROPRODUCTO-------------------------------


@app.route('/mantenimiento-productos', methods=['GET'])
def mantenimientoProductos():
    oSql = CMaestroProducto()
    productos = oSql.select()
    error = oSql.error
    if error:
        return render_template("mantenimientoProductos.html",
                            productos=productos, error=error)
    return render_template("mantenimientoProductos.html",
                            productos=productos)


@app.route('/mantenimiento-productos/select', methods=['POST'])
def selectProductos():
    oSql = CMaestroProducto()
    productos = oSql.select()
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    return json.dumps(productos, cls=AlchemyEncoder)

@app.route('/mantenimiento-productos/select-active', methods=['POST'])
def selectActiveProductos():
    oSql = CMaestroProducto()
    productos = oSql.selectActive()
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    return json.dumps(productos, cls=AlchemyEncoder)

@app.route('/mantenimiento-productos/insert', methods=['POST'])
def insertProducto():
    params = {
        "ccodpro": request.form.get("ccodpro"),
        "cdescri": request.form.get("cdescri"),
        "cunidad": request.form.get("cunidad"),
    }
    oSql = CMaestroProducto()
    oSql.insert(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/mantenimiento-productos/update', methods=['POST'])
def updateProducto():
    params = {
        "ccodpro": request.form.get("ccodpro"),
        "cdescri": request.form.get("cdescri"),
        "cunidad": request.form.get("cunidad"),
    }
    oSql = CMaestroProducto()
    oSql.update(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/mantenimiento-productos/change-state', methods=['POST'])
def changeStateProducto():
    params = {
        "ccodpro": request.form.get("ccodpro")
    }
    oSql = CMaestroProducto()
    oSql.changeState(params)
    error = oSql.error
    return json.dumps({"error": error})

# ------------------------CRUD MAESTROINVENTARIO-------------------------------


@app.route('/bandeja-movimientos', methods=['GET'])
def bandejaMovimientos():
    oSql = CMaestroInventario()
    movimientos = oSql.select()
    error = oSql.error
    if error:
        return render_template("bandejaMovimientos.html",
                           movimientos=movimientos, error=error)
    return render_template("bandejaMovimientos.html",
                           movimientos=movimientos)


@app.route('/bandeja-movimientos/insert', methods=['POST'])
def insertMovimiento():
    params = {
        "ccodsuc": request.form.get("ccodsuc"),
        "cestado": request.form.get("cestado"),
        "dfecha": request.form.get("dfecha"),
        "cdescri": request.form.get("cdescri"),
        "ctipmov": request.form.get("ctipmov")
    }
    oSql = CMaestroInventario()
    oSql.insert(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/bandeja-movimientos/update', methods=['POST'])
def updateMovimiento():
    params = {
        "ccodsuc": request.form.get("ccodsuc"),
        "cestado": request.form.get("cestado"),
        "dfecha":  request.form.get("dfecha"),
        "cdescri": request.form.get("cdescri"),
        "ctipmov": request.form.get("ctipmov")
    }
    oSql = CMaestroInventario()
    oSql.update(params)
    error = oSql.error
    return json.dumps({"error": error})


@app.route('/bandeja-movimientos/delete', methods=['POST'])
def deleteMovimiento():
    params = {
        "ccodpro": request.form.get("ccodpro")
    }
    oSql = CMaestroInventario()
    oSql.delete(params)
    error = oSql.error
    return json.dumps({"error": error})

# ------------------------CRUD DETALLEINVENTARIO-------------------------------


@app.route('/registrar-movimiento', methods=['POST'])
def registrarMovimiento():
    ccodsuc = request.form.get("ccodsuc")
    params = {
        "ccodsuc": ccodsuc,
        "dfecha": request.form.get("dfecha"),
        "cdescri": request.form.get("cdescri"),
        "ctipmov": request.form.get("ctipmov")
    }
    oSql = CMaestroInventario()
    oSql.insert(params)
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    oSql = CMaestroInventario()
    cidmovi = oSql.find()
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    detalles = request.form.get("produc").split(",")
    detallesi = []
    for i in range(0, len(detalles), 3):
        detallesi.append([detalles[i+2], detalles[i+1]])
    for i in range(1, len(detallesi)):
        ccodpro = detallesi[i][0]
        ncantid = detallesi[i][1]
        params = {
            "ccodsuc": ccodsuc,
            "ccodpro": ccodpro,
            "ncantid": ncantid,
            "nstock": "0"
        }

        oSql = CDetalleStock()
        detalle_stock = oSql.find(params)
        if detalle_stock == {}:
            oSql.insert(params)
            error = oSql.error
            if error:
                return json.dumps({"error": error})

        if request.form.get("ctipmov") == "I":
            oSql = CDetalleStock()
            oSql.ingreso(params)
            error = oSql.error
            if error:
                return json.dumps({"error": error})
        elif request.form.get("ctipmov") == "S":
            oSql = CDetalleStock()
            oSql.salida(params)
            error = oSql.error
            if error:
                return json.dumps({"error": error})
        else:
            return json.dumps({"error": error})

        params = {
            "cidmovi": cidmovi,
            "ccodpro": ccodpro,
            "ncantid": ncantid
        }

        oSql = CDetalleInventario()
        oSql.insert(params)
        error = oSql.error
        if error:
            return json.dumps({"error": error})
    return json.dumps({"error": error})

@app.route('/eliminar-movimiento', methods=['POST'])
def eliminarMovimiento():
    params1 = {
        "cidmovi": request.form.get("cidmovi"),
    }
    oSql = CDetalleInventario()
    detalles = oSql.select(params1)
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    
    oSql = CMaestroInventario()
    movimiento = oSql.search(params1)
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    
    for detalle in detalles:
        params2 = {
            "ccodsuc": movimiento.ccodsuc,
            "ccodpro": detalle.ccodpro,
            "ncantid": detalle.ncantid
        }
        if movimiento.ctipmov == "S":
            oSql = CDetalleStock()
            oSql.ingreso(params2)
            error = oSql.error
            if error:
                return json.dumps({"error": error})
        elif movimiento.ctipmov == "I":
            oSql = CDetalleStock()
            oSql.salida(params2)
            error = oSql.error
            if error:
                return json.dumps({"error": error})
        else:
            return json.dumps({"error": error})

        params3 = {
            "nserial": detalle.nserial
        }
        oSql = CDetalleInventario()
        oSql.delete(params3)
        error = oSql.error
        if error:
            return json.dumps({"error": error})
    
    oSql = CMaestroInventario()
    oSql.delete(params1)
    error = oSql.error
    return json.dumps({"error": error})

@app.route('/detalle-movimiento/select', methods=['POST'])
def selectDetalles():
    params = {
        "cidmovi": request.form.get("cidmovi")
    }
    oSql = CDetalleInventario()
    detalles = oSql.find(params)
    error = oSql.error
    if error:
        return json.dumps({"error": error})
    return json.dumps(detalles, cls=AlchemyEncoder)

# ------------------------DETALLE STOCK-------------------------------

@app.route('/detalle-stock', methods=['GET'])
def detalleStock():
    return render_template("detalleStock.html")


@app.route('/detalle-stock/search', methods=['POST'])
def buscarStock():
    ccodsuc = request.form.get("ccodsuc")
    cdescri = request.form.get("cdescri")
    if (not ccodsuc):
        return json.dumps({"error": "PARAMETROS INCORRECTOS"})
    params = {
        "ccodsuc": ccodsuc,
        "cdescri": cdescri
    }
    if params["cdescri"] != "":
        oSql = CDetalleStock()
        productos = oSql.search(params)
        error = oSql.error
    else:
        oSql = CDetalleStock()
        productos = oSql.findSuc(params)
        error = oSql.error
    if error:
            return json.dumps({"error": error})
    return json.dumps(productos, cls=AlchemyEncoder)

#-------------------------------------------------------
