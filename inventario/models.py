from . import db
import json
import datetime
from sqlalchemy import func

# I don't know why, I don't want to know why, I shouldn't
# have to wonder why, but for whatever reason, this works
# without a __repr__.

class MaestroSucursal(db.Model):

    __tablename__ = 'f01msuc'
    ccodsuc = db.Column(db.String(3),
                        primary_key=True)
    cdescri = db.Column(db.VARCHAR(200),
                        index=False,
                        unique=False,
                        nullable=False)
    cestado = db.Column(db.CHAR(1),
                        index=False,
                        unique=False,
                        nullable=False)

class MaestroProducto(db.Model):
    __tablename__ = 'f01mpro'
    ccodpro = db.Column(db.CHAR(8),
                        primary_key=True)
    cdescri = db.Column(db.VARCHAR(200),
                        index=False,
                        unique=False,
                        nullable=False)
    cunidad = db.Column(db.CHAR(3),
                        index=False,
                        unique=False,
                        nullable=False)
    cestado = db.Column(db.CHAR(1),
                        index=False,
                        unique=False,
                        nullable=False)
    tmodifi = db.Column(db.DateTime,
                        index=False,
                        unique=False,
                        nullable=False,
                        server_default=func.now())

class MaestroInventario(db.Model):
    __tablename__ = 'f01minv'
    cidmovi = db.Column(db.INTEGER,
                        primary_key=True)
    ccodsuc = db.Column(db.CHAR(3),
                        db.ForeignKey('f01msuc.ccodsuc'),
                        nullable=False)
    dfecha = db.Column(db.DateTime,
                       index=False,
                       unique=False,
                       nullable=False)
    cdescri = db.Column(db.VARCHAR(200),
                        index=False,
                        unique=False,
                        nullable=False)
    ctipmov = db.Column(db.CHAR(1),
                        index=False,
                        unique=False,
                        nullable=False)

class DetalleInventario(db.Model):
    __tablename__ = 'f01dinv'
    nserial = db.Column(db.INTEGER,
                        primary_key=True)
    cidmovi = db.Column(db.INTEGER,
                        db.ForeignKey('f01minv.cidmovi'),
                        nullable=False)
    ccodpro = db.Column(db.CHAR(8),
                        db.ForeignKey('f01mpro.ccodpro'),
                        nullable=False)
    ncantid = db.Column(db.NUMERIC(6, 2),
                        index=False,
                        unique=False,
                        nullable=False)


class DetalleStock(db.Model):
    __tablename__ = 'f01dsto'
    ccodsuc = db.Column(db.CHAR(3),
                        db.ForeignKey('f01msuc.ccodsuc'),
                        primary_key=True)
    ccodpro = db.Column(db.CHAR(8),
                        db.ForeignKey('f01mpro.ccodpro'),
                        primary_key=True)
    nstock = db.Column(db.NUMERIC(6, 2),
                       index=False,
                       unique=False,
                       nullable=False)
