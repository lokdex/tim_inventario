"""App entry point."""
from inventario import create_app
from livereload import Server

app = create_app()

app.debug = True


server = Server(app.wsgi_app)
server.serve()


if __name__ == "__main__":
    app.run(host='0.0.0.0')
