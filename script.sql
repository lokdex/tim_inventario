-- BASE DE DATOS PARA EL MÓDULO DE INVENTARIO

-- TABLA MAESTRO DE SUCURSALES
CREATE TABLE f01msuc (

	ccodsuc		character(3) PRIMARY KEY,
	cdescri		varchar(200) NOT NULL,
	cestado		character(1) NOT NULL,

);

-- TABLA MAESTRO DE PRODUCTOS
CREATE TABLE f01mpro (

	ccodpro		character(8) PRIMARY KEY,
	cdescri		varchar(200) NOT NULL,
	cunidad		character(3) NOT NULL,
	cestado		character(1) NOT NULL,
	tmodifi		timestamp without time zone DEFAULT now()
	
);

-- TABLA MAESTRO DE MOVIMIENTOS DE INVENTARIO (KARDEX)
CREATE TABLE f01minv (

	cidmovi		serial PRIMARY KEY,
	ccodsuc		character(3) REFERENCES f01msuc(ccodsuc),
	dfecha		timestamp without time zone DEFAULT now(),
	cdescri		varchar(200) NOT NULL,
	ctipmov		character(1) NOT NULL

);

-- TABLA DETALLE DE MOVIMIENTOS DE INVENTARIO (DETALLE KARDEX)
CREATE TABLE f01dinv (

	nserial		serial PRIMARY KEY,
	cidmovi		integer REFERENCES f01minv(cidmovi),		
	ccodpro		character(8) REFERENCES f01mpro(ccodpro),
	ncantid		numeric(6,2) NOT NULL

);

CREATE TABLE f01dsto (

	ccodsuc		character(3) REFERENCES f01msuc(ccodsuc),
	ccodpro		character(8) REFERENCES f01mpro(ccodpro),
	nstock		numeric(6,2) NOT NULL,
	PRIMARY KEY (ccodsuc, ccodpro)

);